require 'byebug'
# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
# arr = [[5,4,3,2,1],[],[]]
# A larger disc must always come before a smaller disc.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi

  attr_reader :towers
  #__________________________________________________________
  #                       The Functional methods

  def initialize
    #puts "Welcome to Towers of Hanoi!"
    @towers = [[3,2,1],[],[]] #Could make this at random with a 'for i in 0..2' loop
  end

  def move(from_tower, to_tower)
    if valid_move?(from_tower,to_tower)
      @towers[to_tower] << @towers[from_tower].pop
    end
  end

  def valid_move?(from_tower,to_tower)
    return false if @towers[from_tower] == []
    return true if @towers[to_tower] == [] #Important step here.
    @towers[to_tower].last > @towers[from_tower].last
  end

  def won? #When all three towers are in index 1 || 2
    @towers[1].length == 3 || @towers[2].length == 3
  end

#__________________________________________________________
#                       The Visual Methods

  def render
    puts "Towers Rendered! Larger discs are on the bottom."
    puts ""
    puts  "Layout:"
    display_information
  end

  def display_information
      puts "_______________________"
      puts "Column One  : " + @towers[0].to_s
      puts "Column Two  : " + @towers[1].to_s
      puts "Column Three: " + @towers[2].to_s
      puts "_______________________"
  end

  def play

      render

      until won?
        #  byebug
          puts "From which tower would you like move?(1,2, or 3): "
          @from_tower = gets.chomp.to_i - 1
          puts "To which tower would you like the move(1,2,or3): "
          @to_tower = gets.chomp.to_i - 1

          self.move(@from_tower,@to_tower)

          display_information

      end

      puts "Congratulations! You Won!"

  end

end
